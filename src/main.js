import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//全局加载element组件库
import ElementUI from 'element-ui'
//全局加载element组件库样式
import 'element-ui/lib/theme-chalk/index.css'
//按需导入element-ui
import './element-ui/index.js'
//导入全局样式表
import './assets/css/global.css'
//导入字体图标
import './assets/fonts/iconfont.css'
//导入axios
import axios from 'axios'
//导入qs
import qs from 'qs'

//挂载axios
Vue.prototype.$axios = axios
//挂载qs
Vue.prototype.$qs = qs
//设置axios基础url路径
axios.defaults.baseURL = 'http://localhost:8000/'

//在axios请求头对象中挂载 Authorization 字段，值为保存的token。
axios.interceptors.request.use( config => {
  config.headers.Authorization = window.sessionStorage.getItem('token');
  console.log(config);
  return config;
})

//全局注册element组件库
Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
