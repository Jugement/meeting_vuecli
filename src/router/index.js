import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Conference from '../components/Conference.vue'
import Users from '../components/Users.vue'
import Rooms from '../components/Rooms.vue'
import Records from '../components/Records.vue'
import Repair from '../components/Repair.vue'
import Repaired from '../components/Repaired.vue'
import UpdPwd from '../components/UpdPwd.vue'
import Register from '../components/Register.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/home',
    component: Home,
    redirect: '/conference',
    children: [
      {
        path: '/conference',
        component: Conference
      },
      {
        path: '/users',
        component: Users
      },
      {
        path: '/rooms',
        component: Rooms
      },
      {
        path: '/records',
        component: Records
      },
      {
        path: '/repair',
        component: Repair
      },
      {
        path: '/repaired',
        component: Repaired
      },
      {
        path: '/updPwd',
        component: UpdPwd
      }
    ]
  },
  {
    path: '/register',
    component: Register
  }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫
router.beforeEach( (to, from, next) => {
  if(to.path === '/login' || to.path === '/register') return next();
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token');
  if(!tokenStr) return next('/login')
  next();
})

export default router
